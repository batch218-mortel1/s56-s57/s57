


let records = [
  {
    id: 1,
    name: 'Brandon',
    subjects: [
      { name: 'English', grade: 98 },
      { name: 'Math', grade: 66 },
      { name: 'Science', grade: 87 }
    ]
  },
  {
    id: 2,
    name: 'Jobert',
    subjects: [
      { name: 'English', grade: 87 },
      { name: 'Math', grade: 99 },
      { name: 'Science', grade: 74 }
    ]
  },
  {
    id: 3,
    name: 'Junson',
    subjects: [
      { name: 'English', grade: 60 },
      { name: 'Math', grade: 99 },
      { name: 'Science', grade: 87 }
    ]
  }
];

console.log(records[2].subjects[0].grade)